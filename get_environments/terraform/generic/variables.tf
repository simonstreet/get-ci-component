variable "prefix" {
  default = null
}

variable "project" {
  default = null
}

variable "region" {
  default = null
}

variable "zone" {
  default = null
}

variable "external_ip" {
  default = null
}

variable "postgres_password" {
  default = null
}

variable "object_storage_location" {
  default = null
}

variable "service_account_prefix" {
  default = null
}
