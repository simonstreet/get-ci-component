# get-ci-component

This project contains two [CI/CD Components](https://docs.gitlab.com/ee/ci/components/)
- [get-environment](templates/get-environment/docs.md)
   - Allows the deployment of environments with [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit)
   - Quite an amount of effort is required to get this setup but once done you can deploy an environment with just a terraform file
- [variable-cleanup](templates/variable-cleanup/docs.md)
   - Cleans up environment scoped CI/CD variables
   - Configurable regex for:
      - `environment_scope_regex` - Environment names to include in the cleanup
      - `ignore_variables` - Variables names/keys to ignore
