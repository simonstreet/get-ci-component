# GitLab Environment Toolkit CI Component

[[_TOC_]]

# Introduction

What this project is **not**:
- Suitable for production
- Supported by GitLab
- Entirely plug and play, there is a lot of work to do to get going the first time

What it **is**:
- A learning experience for me (and you) for:
   - Environments
   - Terraform integration
   - GCP API
   - More advanced CI/CD scripting
   - CI/CD Components
- Intended for use by GitLab Team Members to quickly deploy/destroy environments for testing
   - Get setup on GCP via [Sandbox Cloud](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)
- An attempt to automate as much as possible away from the user so you only need to place a `environment.tf` file and run a pipeline (the `generic` method)

Whilst each stage is split in to a unique file they are intended to be used as a whole rather than individually, as such there is only one template file.

# TODO/Wish list

## GET Support

Currently supported by the `generic` method:
- [x] Pure Omnibus environments
- [x] Google Memorystore (Redis)
- [x] Google Cloud SQL

Currently unsupported by the `generic` method:
- [ ] Hybrid environments
   - I attempted this but something wasn't working. I likely need to get it going properly to debug it as the whole pipeline takes ages to run and it's not ideal for debugging
- [ ] Anything else that requires specific Terraform/Ansible manual additions to files

You should be able to duplicate the `generic` files and make modifications by hand to deploy other configurations without relying on the automation in this Component.

This should work if you create all of the necessary files per the GET documentation however my goal is to automate away that requirement

# GETting Setup

## Prerequisites

If you choose to house this in a public project I would consider ensuring your pipeline job logs are not public. This project has been designed with security in mind (and it is only for testing!) however it is possible that a secret may end up in an error or similar.

### DNS Delegation

In order to automate everything we need to (sub)delegate a domain name to GCP so that the scripts can handle DNS and certificate generate. A neat trick here is you can delegate a subdomain to different name servers, this means you do not have to dedicate a whole domain to this project! As an example:

- If you own `example.com` and you already have that setup somewhere with a website etc you can use `get.example.com`
- Follow the [GCP DNS quick start](https://cloud.google.com/dns/docs/set-up-dns-records-domain-name/) documentation to add the domain, use `get.example.com` as the `DNS Name`.
   - Make a note of the `Zone Name` you choose for `GCLOUD_DNS_ZONE` in `.gitlab-ci.yml`
   - Add an A record so you can test later on, doesn't need to be "valid". e.g. `test.get.example.com` => `127.0.0.1`
- Look up the [name servers](https://cloud.google.com/dns/docs/update-name-servers) for the zone you just created
- Log in your DNS provider for `example.com` and add some `NS` records, for example:
   - If your name servers are:
      - ns-cloud-a1.googledomains.com
      - ns-cloud-a2.googledomains.com
      - ns-cloud-a3.googledomains.com
      - ns-cloud-a4.googledomains.com
   - Add a record for each name server:
     - Type: `NS`
     - Name: `get` or `get.example.com` (Varies on interface/provider. Look at the existing records to see if the FQDN is required.)
     - Content: `ns-cloud-a1.googledomains.com`
     - TTL: Up to you, I would advise setting this lower (5 minutes) while you are testing/setting up this bit until you are happy it is working then you can increase it.
- Try and lookup the test record you setup earlier, if this errors then something is likely wrong. (Or something is cached somewhere)
  - `ping test.get.example.com` - simplist method
  - `nslookup test.get.example.com`
  - `dig -t A example.com`
- I like the [squish.net DNS traversal checker](https://dns.squish.net/) for visual diagnostics of DNS.
   - Enter `test.get.example.com` and type A (or whatever you chose to setup earlier) and complete the CAPTCHA to get a detailed report on the DNS tree

### Generate an SSH key pair

Generate an SSH key pair without a password:

```shell
ssh-keygen -t ed25519 -C get_environments -f ~/.ssh/get_environments
```
Create some project level CI/CD variables:

- `SSH_PRIVATE_KEY` as a `File` containing the contents of `~/.ssh/get_environments`
- `SSH_PUBLIC_KEY` as a `File` with the contents of `~/.ssh/get_environments.pub`

### Create a GitLab API token

Personally I have used a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) however depending on your license/environment you may not have access to these. This token needs quite high access as we will be manipulating CI/CD variables with it.

- Create an API token with `api` scope and as `owner` and expiry date you are comfortable with
- Create a masked CI/CD variable called `API_TOKEN` containing the token

### Create a gcloud service account

- Follow the [GET - Preparing the environment - GCP](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#3-setup-provider-authentication-gcp-service-account) guide
- Add the following extra roles:
   - `Compute Network Admin` (So we can create/delete IP addresses)
   - `DNS Administrator` (So `acme.sh` can do it's thing and we can create DNS records)
   - You will need to enable the relevant APIs as well (not a conclusive list, I likely had some APIs already enabled)
     - Google Cloud Memorystore for Redis API
     - Cloud Resource Manager API
     - Service Networking API
     - Cloud SQL Admin API
- Create/download the service account key 
- Create some masked project level CI/CD variables:
   - `GCLOUD_SERVICE_ACCOUNT_EMAIL` as a `Variable` with the service account email address
   - `GCLOUD_SERVICE_ACCOUNT_KEYFILE` as a `File` containing the json file you just downloaded (cannot be masked)
   - `GCLOUD_PROJECT_ID` as a `Variable` with your project ID
   - `SSH_USERNAME` as a `Variable` with the `sa_` username from https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#4-setup-ssh-authentication-ssh-os-login-for-gcp-service-account

### Add your license

Create a project level CI/CD variable:
- `GITLAB_LICENSE` as a `File` containing your license key

### Add generic GET files

This is optional, but it reduces the tasks needed to add an environment to your project in the future.

#### Quick version

Download/clone the CI Component repository and copy the `get_environments` directory to your project.

#### Terraform

Due to how GET is [architectured](https://gitlab.com/gitlab-org/gitlab-environment-toolkit#how-it-works) and how Terraform works we can abstract the `main.tf` and `variables.tf` files to a generic location and we only need to provide the `environments.tf` file for anything we want to deploy.

With this in mind in your project create a `get_environments/terraform/generic` directory and duplicate the `main.tf` and `variables.tf` files from the CI Component's `get_environments/terraform/generic` directory.

#### Ansible

Similarly we can use shared versions of the ansible files.

Create a `get_environments/anisble/generic/inventory` directory and duplicate the `all.yml` and `generic.gcp.yml` files files from the CI Component's `get_environments/anisble/generic/inventory` directory.

If you are using a staging license then it's a good idea to also create the custom configuration directory, `get_environments/anisble/generic/files/gitlab_configs`, and add a `gitlab_rails.rb.j2` file with the necessary configuration. This is included in the CI Component project.

## CI configuration 

There are a large amount of tunable variables in this project, for now the base `.gitlab-ci.yml` you will need is

```yaml
include:
  - component: gitlab.com/simonstreet/get-ci-component/get-environment@main
    inputs:
      environment_name:
      gcloud_domain_name:
      gcloud_domain_zone:
      gcloud_region:
      gcloud_zone:
```

This will perform the certificate creation stage against the LetsEncrypt staging server to help you avoid rate limits if something is wrong. Once you are happy the staging certificates are being issued (and if you want a valid certificate) you can:
- Add `acmesh_server: letsencrypt` to the list of `inputs:`
- Delete one of the `SSL_*` CI/CD variables for the environment you tested with
  - You can delete all four if you wish but deleting one will trigger a reissue

Once you have more than one environment in your repository you might want to consider something like this:

```yaml
variables:
  GET_ENVIRONMENT_NAME:
    description: Environment name to run pipeline for
    value: ''
    options:
      - ''
      - 'environment1'
      - 'environment2'
      - 'environment3'
  GET_ENVIRONMENT_AUTOSTOP:
    description: Auto stop time for environment
    value: '8 hours'
  GITLAB_VERSION:
    description: GitLab Version
    value: latest

workflow:
  rules:
    # Run if we have an environment name
    - if: $GET_ENVIRONMENT_NAME != ""
    # Dummy job to stop errors on commits
    - if: $CI_COMMIT_BRANCH

include:
  - component: gitlab.com/simonstreet/get-ci-component/get-environment@main
    inputs:
      environment_name: "${GET_ENVIRONMENT_NAME}"
      environment_autostop_time: "${GET_ENVIRONMENT_AUTOSTOP}"
      gcloud_domain_name:
      gcloud_domain_zone:
      gitlab_version: ${GITLAB_VERSION}
      acmesh_server: "letsencrypt"
      gcloud_region:
      gcloud_zone:
    rules:
     - if: $GET_ENVIRONMENT_NAME != ""

dummy:
  image: alpine:latest
  script:
    - echo dummy
  rules:
    - if: $NEVER_EXISTS
```

With this configuration you can start a manual pipeline and pick the 

## Required Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `environment_name`  |  | GET Environment Name - Used to load Terraform/Ansible files, set GitLab Environment name, and to derive other variables. Should be a maximum of 10 charaters long, can contain `-` and alphanumerics. Must start with a letter and cannot end with a `-` |
| `gcloud_domain_name`  |  | Base domain name added/delegated to GCP |
| `gcloud_domain_zone`  |  | Name of the Zone matching `domain_name` in GCP |
| `gcloud_region`  |  | GCP Region to use for deployment |
| `gcloud_zone`  |  | GCP Zone to use for deployment (where possible) |

## Optional Inputs

Default values in `code quotes` are references to other inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `get_version` | 3.3.2 | Gitlab Environment Toolkit version (used as tag for `git_image` container) |
| `get_image` | registry.gitlab.com/gitlab-org/gitlab-environment-toolkit | GitLab Environment Toolkit container URL |
| `environment_autostop_time` | 8 hours | Autostop time for GitLab Environment |
| `gitlab_version` | latest | GitLab version to deploy in `x.y.z` format |
| `dns_base` | `environment_name`.`gcloud_domain_name` | Base domain name for Environment |
| `dns_gitlab` | gitlab.`dns_base` | Domain name for GitLab |
| `dns_registry` | registry.`dns_base` | Domain name for Registry |
| `certificate_domains` | `dns_base` *.`dns_base` | Domain(s) to include on the certificate we issue as a [space] delimited list |
| `acmesh_server` | letsencrypt_test | acme.sh certificate server |
| `acmesh_renewthreshold` | 2592000 | Time in seconds before cached certificate is expired to attempt renewal |
| `terraform_version` | 1.5.0 | Version of terraform to use (passed to msie) |
| `password_length` | 30 | Length of any generated passwords. |
| `gcloud_ip_region` | `gcloud_region` | GCP Region to create our IP address in |
| `gcloud_ip_network_tier` | standard | GCP Network Tier for IP address |
| `gcloud_ip_name` | `environment_name` | Name for the IP address we create |
| `gcloud_object_storage_location` | `gcloud_region` | Location to create storage buckets in |
| `gcloud_service_account_prefix` | `environment_name` | Service account prefix. Should be a maximum of 10 charaters long, can contain `-` and alphanumerics. Must start with a letter and cannot end with a `-` |

## Very Optional Inputs

These are **NOT** where you store your configuration files, but where the scripts place them/operate from (you probably don't need to touch these).

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `terraform_dir_base` | /gitlab-environment-toolkit/terraform | Directory for Terraform files |
| `terraform_dir_environments` | `terraform_dir_base`/environments | Directory for Terraform environments |
| `terraform_dir_environment_name` | `terraform_dir_environments`/`get_environment_name` | Directory for specific environment Terraform files |
| `ansible_dir_base` | /gitlab-environment-toolkit/ansible | Directory for Ansible files |
| `ansible_dir_environments` | `ansible_dir_base`/environments' | Directory for Ansible environments |
| `ansible_dir_environment_name` | `ansible_dir_environments`/`get_environment_name` | Directory for specific environment ansible files |


# Deploying an environment

If you have followed all of the above and got here, well done!

The final step is creating your environment specific `environment.tf` file.

TODO: naming limitations

- Create a `git_environments/terraform/ENVIRONMENT_NAME` directory
- Create a `environment.tf` file following the [documentation](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#configure-module-settings-environmenttf)
   - Make sure you include the following to allow key variables to make it through to GCP
   ```tf
   module "gitlab_ref_arch_gcp" {
     source = "../../modules/gitlab_ref_arch_gcp"

     prefix                  = var.prefix
     project                 = var.project
     postgres_password       = var.postgres_password
     object_storage_location = var.object_storage_location
     service_account_prefix  = var.service_account_prefix

     # Node defintions below
   }

   ```
- Ensure your [inputs](#required-inputs) are correct
   - Check [above](#ci-configuration) for suggestions on handling multiple environments
- Head to Build>Pipelines, click `Run pipeline`, if necessary check the variables, and click `Run pipeline`

You will need to get the root password from the CI/CD variables in the project, look for the one scoped to the environment name and edit it to see the password.

# Troubleshooting

Honestly I think it's going to be hard to cover common errors given the moving parts but most of the tools involved are fairly verbose if they fail.

Feel free to reach out on Slack or open an issue here and I will do my best to assist.

## Problem deploying with external Redis (and probably Postgresql as well)

If you have manually cleaned up a failed deployment you may run in to this error when deploying next time:

```
Error waiting for Create Service Networking Connection: Error code 9, message: Cannot modify allocated ranges in CreateConnection. Please use UpdateConnection.
```

See:
- https://github.com/hashicorp/terraform-provider-google/issues/16697
- https://github.com/GoogleCloudPlatform/magic-modules/pull/11364

Further googling suggests using `PATCH` rather than `POST` may get around this but TODO testing and maybe a MR to GET.

If you haven't tinkered too much then you want to run:

```
gcloud services vpc-peerings update --service=servicenetworking.googleapis.com --ranges="ENVIRONMENT_NAME-private-service-ip-range" --network="default" --project="GOOGLE_PROJECT_ID" --force
```

Then rerun the `terraform_build` and `terraform_deploy` jobs.
