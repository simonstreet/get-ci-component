# Ansible stages

# Hidden job, used via `extends:` to ensure our environment is correct
.ansible_base:
  image: ${GET_IMAGE}:${GET_VERSION}
  variables:
    # https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_configure.md#configure-authentication-gcp
    GCP_SERVICE_ACCOUNT_FILE: ${GCLOUD_SERVICE_ACCOUNT_KEYFILE}
    GCP_AUTH_KIND: serviceaccount
  before_script:
    - !reference [.setup_defaults, script]
    - !reference [.get_setup, script]
    - !reference [.gcloud_setup, script]
    - cd ${ANSIBLE_DIR_BASE}
    - |
      # Check if we have a specific environment inventory, otherwise use the generic one
      # TODO Geo
      if [[ -d ${ANSIBLE_DIR_BASE}/environments/${GET_ENVIRONMENT_NAME}/inventory ]]; then
        ANSIBLE_INVENTORY="environments/${GET_ENVIRONMENT_NAME}/inventory"
      else
        ANSIBLE_INVENTORY="environments/generic/inventory"

        # If we're using the generics we need to backfil some settings
        # Check/add memorystore configuration if required
        # Check for a single Redis
        if [[ ! -z ${MEMORYSTORE_PASSWORD+x} ]]; then
          echo "Memorystore configuration detected, appending to ansible vars"
          # add a blank line in case there is no trailing one in the file
          echo >> ${ANSIBLE_INVENTORY}/all.yml
          echo "    redis_password: ${MEMORYSTORE_PASSWORD}" >> ${ANSIBLE_INVENTORY}/all.yml
          echo "    redis_host: ${MEMORYSTORE_HOST}" >> ${ANSIBLE_INVENTORY}/all.yml
        fi
        # Check for multi redis, cache is always(?) required so
        if [[ ! -z ${MEMORYSTORE_CACHE_PASSWORD+x} ]]; then
          echo "Memorystore configuration detected, appending to ansible vars" 
          # Hopefully if GET allows other persistent configurations in the future we can just change this list
          # add a blank line in case there is no trailing one in the file
          echo >> ${ANSIBLE_INVENTORY}/all.yml
          MEMORYSTORE_TYPES="cache persistent"
          for TYPE in ${MEMORYSTORE_TYPES}; do
            MEMORYSTORE_VARIABLES="password host"
            for VAR in ${MEMORYSTORE_VARIABLES}; do
              TMP=MEMORYSTORE_${TYPE^^}_${VAR^^}
              echo "    redis_${TYPE}_${VAR}: ${!TMP}" >> ${ANSIBLE_INVENTORY}/all.yml
            done
          done
        fi

        # Check/setup external DB if required
        if [[ ! -z ${POSTGRES_HOST} ]]; then
          echo "External postgresql detected, apending to ansible vars"
          # add a blank line in case there is no trailing one in the file
          echo >> ${ANSIBLE_INVENTORY}/all.yml
          echo "    postgres_host: ${POSTGRES_HOST}" >> ${ANSIBLE_INVENTORY}/all.yml
          echo "    postgres_admin_username: postgres" >> ${ANSIBLE_INVENTORY}/all.yml
          echo "    postgres_admin_password: ${POSTGRES_PASSWORD}" >> ${ANSIBLE_INVENTORY}/all.yml
        fi
      fi
      echo "Using ${ANSIBLE_INVENTORY} as inventory"
    - |

  environment:
    name: ${GET_ENVIRONMENT_NAME}

# Ping our inventory to give a visual check things are working
ansible_ping:
  extends: .ansible_base
  stage: ansible_lint
  script:
    - ansible all -m ping -i ${ANSIBLE_INVENTORY} --list-hosts

# Detect Memorystore (Redis) and setup CI/CD Variables as needed
ansible_memorystore:
  extends: .ansible_base
  stage: ansible_lint
  script:
    - |
      # Look at the environment file for memorystore configuration
      # The `cut` on this `grep` is for cases where we have more than one Redis service, but it still helps us in a single service setup
      RES=`grep "memorystore.*count" ${TF_DIR_ENVIRONMENT_NAME}/environment.tf | cut -d_ -f3` || RET=$?
      if [[ $RET -eq 0 ]]; then
        echo "Found Memorystore node(s)"
        COUNT=`echo "${RES}" | wc -l`
        # GET only supports the configurations noted in the RAs, which is a single Redis, or two (cache+persistent)
        # GitLab supports more than this (https://docs.gitlab.com/ee/administration/redis/replication_and_failover.html#running-multiple-redis-clusters)
        # An attempt is made to future proof this
        # Information will be pushed in to CI/CD Variables as dotenv is probably(?) prone to race conditions here

        # The information for this step is output in the `terraform_deploy` stage but I figure it's easier to just get it out of GCP
        # https://cloud.google.com/memorystore/docs/redis/manage-redis-auth#getting_the_auth_string
        if [[ ${COUNT} -eq 1 ]]; then
          echo "Saving single node Redis password/host"
          PASSWORD=`gcloud redis instances get-auth-string ${GET_ENVIRONMENT_NAME}-redis --region ${GCLOUD_REGION} | cut -d\  -f2`
          curl -s --request POST \
            --header "PRIVATE-TOKEN: ${API_TOKEN}" \
            --form "key=MEMORYSTORE_PASSWORD" \
            --form "value=${PASSWORD}" \
            --form "variable_type=env_var" \
            --form "protected=false" \
            --form "masked=true" \
            --form "raw=true" \
            --form "description=Managed by CI/CD" \
            --form "environment_scope=${GET_ENVIRONMENT_NAME}" \
            "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables" > /dev/null

            HOST=`gcloud redis instances describe ${GET_ENVIRONMENT_NAME}-redis --region ${GCLOUD_REGION} | grep '^host:' | cut -d\  -f2`
            curl -s --request POST \
              --header "PRIVATE-TOKEN: ${API_TOKEN}" \
              --form "key=MEMORYSTORE_HOST" \
              --form "value=${HOST}" \
              --form "variable_type=env_var" \
              --form "protected=false" \
              --form "masked=true" \
              --form "raw=true" \
              --form "description=Managed by CI/CD" \
              --form "environment_scope=${GET_ENVIRONMENT_NAME}" \
              "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables" > /dev/null
          else
            for SUFFIX in ${RES}; do
              echo "Found a ${SUFFIX} memory store configuration, saving password/host"
              PASSWORD=`gcloud redis instances get-auth-string ${GET_ENVIRONMENT_NAME}-redis-${SUFFIX} --region ${GCLOUD_REGION} | cut -d\  -f2`
              curl -s --request POST \
                --header "PRIVATE-TOKEN: ${API_TOKEN}" \
                --form "key=MEMORYSTORE_${SUFFIX^^}_PASSWORD" \
                --form "value=${PASSWORD}" \
                --form "variable_type=env_var" \
                --form "protected=false" \
                --form "masked=true" \
                --form "raw=true" \
                --form "description=Managed by CI/CD" \
                --form "environment_scope=${GET_ENVIRONMENT_NAME}" \
                "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables" > /dev/null
              HOST=`gcloud redis instances describe ${GET_ENVIRONMENT_NAME}-redis-${SUFFIX} --region ${GCLOUD_REGION} | grep '^host:' | cut -d\  -f2`
              curl -s --request POST \
                --header "PRIVATE-TOKEN: ${API_TOKEN}" \
                --form "key=MEMORYSTORE_${SUFFIX^^}_HOST" \
                --form "value=${HOST}" \
                --form "variable_type=env_var" \
                --form "protected=false" \
                --form "masked=true" \
                --form "raw=true" \
                --form "description=Managed by CI/CD" \
                --form "environment_scope=${GET_ENVIRONMENT_NAME}" \
                "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables" > /dev/null
          done
        fi
      else
        echo "No Memorystore configured"
      fi

# Detect external postgresql and setup CI/CD Variables as needed
ansible_external_postgres:
  extends: .ansible_base
  stage: ansible_lint
  script:
    - |
      # Look at the environment file for cloud_sql configuration
      RES=`grep "cloud_sql_postgres" ${TF_DIR_ENVIRONMENT_NAME}/environment.tf` || RET=$?
      if [[ $RET -eq 0 ]]; then
        # Terraform config has an external DB specified, find the hostname
        echo "Found external postgres, saving hostname"
        HOST=`gcloud sql instances describe ${GET_ENVIRONMENT_NAME}-cloud-sql | grep "ipAddress:" | cut -d\  -f3`
        curl -s --request POST \
          --header "PRIVATE-TOKEN: ${API_TOKEN}" \
          --form "key=POSTGRES_HOST" \
          --form "value=${HOST}" \
          --form "variable_type=env_var" \
          --form "protected=false" \
          --form "masked=true" \
          --form "raw=true" \
          --form "description=Managed by CI/CD" \
          --form "environment_scope=${GET_ENVIRONMENT_NAME}" \
          "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables" > /dev/null
      else
        echo "No external postgresql configure"
      fi


# Individual jobs for each stage of `playbooks/all.yml` to avoid the 1 hour job timeout.
# This does result in some duplication of tasks but I have found sometimes an hour is not enough
# We use `needs:job` to ensure some sense of order
ansible_deploy_common:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/common.yml

ansible_deploy_haproxy:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/haproxy.yml
  needs:
  - job: ansible_deploy_consul

ansible_deploy_gitlab_nfs:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/gitlab_nfs.yml
  needs:
  - job: ansible_deploy_consul

ansible_deploy_consul:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/consul.yml
  needs:
  - job: ansible_deploy_common

ansible_deploy_postgres:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/postgres.yml
  needs:
  - job: ansible_deploy_consul

ansible_deploy_pgbouncer:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/pgbouncer.yml
  needs:
  - job: ansible_deploy_postgres

ansible_deploy_redis:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/redis.yml
  needs:
  - job: ansible_deploy_haproxy
  - job: ansible_deploy_pgbouncer

ansible_deploy_gitaly:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/gitaly.yml
  needs:
  - job: ansible_deploy_consul

ansible_deploy_praefect_postgres:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/praefect_postgres.yml
  needs:
  - job: ansible_deploy_consul

ansible_deploy_praefect:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/praefect.yml
  needs:
  - job: ansible_deploy_praefect_postgres

ansible_deploy_gitlab_rails:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/gitlab_rails.yml
  needs:
  - job: ansible_deploy_haproxy
  - job: ansible_deploy_gitlab_nfs
  - job: ansible_deploy_pgbouncer

ansible_deploy_sidekiq:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/sidekiq.yml
  needs:
  - job: ansible_deploy_haproxy
  - job: ansible_deploy_gitlab_nfs
  - job: ansible_deploy_pgbouncer

ansible_deploy_monitor:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/monitor.yml
  needs:
  - job: ansible_deploy_consul
  - job: ansible_deploy_pgbouncer
  - job: ansible_deploy_praefect
  
ansible_deploy_gitlab_charts:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/gitlab_charts.yml
  needs:
  - job: ansible_deploy_consul
  - job: ansible_deploy_haproxy
  - job: ansible_deploy_pgbouncer

ansible_deploy_opensearch:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/opensearch.yml
  needs:
  - job: ansible_deploy_gitlab_rails
  - job: ansible_deploy_sidekiq
  - job: ansible_deploy_gitlab_charts

ansible_deploy_post_configure:
  extends: .ansible_base
  stage: ansible_deploy
  script:
  - ansible-playbook ${ANSIBLE_EXTRA_OPTS} -i ${ANSIBLE_INVENTORY} playbooks/post_configure.yml
  needs:
  - job: ansible_deploy_opensearch
