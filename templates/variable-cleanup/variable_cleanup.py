import re
import os
import gitlab
import argparse

from time import sleep

# Parse CLI options
parser = argparse.ArgumentParser(description='CI/CD Variable Cleanup Tool')
parser.add_argument("--environment-scope-regex", help='regex of environment scopes to include', required=True)
parser.add_argument("--ignore-variables", help='regex of variable names to ignore', required=True)
parser.add_argument("--dry-run", help='only output what would be deleted', action='store_true')
args = parser.parse_args()

# Setup re parsers
env_scope_regex = re.compile(args.environment_scope_regex)
ignore_vars_regex = re.compile(args.ignore_variables)

# Setup gitlab API
gl = gitlab.Gitlab(url=os.environ['CI_SERVER_URL'], private_token=os.environ['API_TOKEN'])
gl.auth()
api_sleep = 0.1

project = gl.projects.get(os.environ['CI_PROJECT_ID'])

if args.dry_run:
    print("Running in DRY RUN mode, no changes will be made.")
else:
    print("** Running in LIVE mode, changes will be made. **")

for variable in project.variables.list(get_all=True):
    if env_scope_regex.match(variable.environment_scope) and not ignore_vars_regex.match(variable.key):
        print("Deleting variable: " + variable.key + " from environment: " + variable.environment_scope + "...")

        if not args.dry_run:
            variable.delete(filter={'environment_scope': variable.environment_scope})
            sleep(api_sleep)
