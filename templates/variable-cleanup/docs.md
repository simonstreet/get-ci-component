# Clean up project level CI/CD variables with GitLab CI/CD

Provides a Python script using [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) to delete CI/CD variables based on regex patterns.

[[_TOC_]]

## variable-cleanup

```yaml
include:
  - component: gitlab.com/simonstreet/get-ci-component/variable-cleanup@main
    inputs:
      stage: test
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage`  |  | The pipeline stage to run this job in |
| `environment_scope_regex` | ^(\w\|\ \|-\|/\|\$\|{\|})*$ | Regex to filter environment_scope. Default matches any [valid name](https://docs.gitlab.com/ee/ci/yaml/#environmentname) |
| `ignore_variables` | ^(SSL).*$ | Regex of variables to ignore. Default excludes variables starting with `SSL` |
| `dry_run` | false | Perform a dry run |
| `script_url` | https://gitlab.com/simonstreet/get-ci-component/-/raw/main/templates/variable-cleanup/variable_cleanup.py | URL of `script_file` to download if needed |
| `script_file` | variable_cleanup.py | Python script download from `script_url` (if missing) and execute |
